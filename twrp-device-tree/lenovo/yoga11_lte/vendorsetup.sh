#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

add_lunch_combo omni_yoga11_lte-user
add_lunch_combo omni_yoga11_lte-userdebug
add_lunch_combo omni_yoga11_lte-eng
