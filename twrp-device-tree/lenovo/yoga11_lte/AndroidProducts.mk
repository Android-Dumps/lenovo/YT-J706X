#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_yoga11_lte.mk

COMMON_LUNCH_CHOICES := \
    omni_yoga11_lte-user \
    omni_yoga11_lte-userdebug \
    omni_yoga11_lte-eng
